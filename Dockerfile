### Stage 1 - build the app
FROM node:10-alpine as builder
WORKDIR /app

COPY ./package.json ./package-lock.json /app/
RUN npm install
COPY . .

RUN NODE_ENV="production" npm run build

### Stage 2 - run the app
FROM nginx:1.10.1-alpine

RUN mkdir /app
COPY --from=builder /app/dist/pexapark-test /app
COPY nginx/app-frontend.conf /etc/nginx/conf.d/
RUN rm /etc/nginx/conf.d/default.conf

WORKDIR /etc/nginx
EXPOSE 80

CMD nginx -g 'daemon off;'
