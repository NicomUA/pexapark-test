# PexaparkTest
Test task for pexapark.  

#Environment
- Angular 8 CLI
- Node >=10
- Docker (for prod image build)

## Setup
Run `npm install`

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `npm run build`.

## Build Prod
Run `npm run build-prod` to build image. 

## Docker build for
Docker image contain nginx. Config for it `nginx/app-frontend.conf`.
To build it run `docker build -t pexapark-web .` for image.
To run `docker run -p 8080:80 pexapark-web`  and open `http://localhost:8080`.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
All test runs in puppeteer (headless chrome) environment. CLI will show only error. To change that edit `./karma.conf.js` file.  
