import { Component, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data-service.service';
import { powerStation } from 'src/app/services/data-service';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  title = 'Pexapark test';
  selectedId: number;
  plants: powerStation[]

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private dataService: DataService) {
    this.dataService.getPowerStations().subscribe(plants => this.plants = plants);
  }

  switchPlant() {
    this.dataService.setCurrentStation(this.selectedId);
  }
}
