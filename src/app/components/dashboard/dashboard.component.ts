import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data-service.service';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  chart: Chart
  chartOptions: Highcharts.Options = {
    chart: {
      type: 'column',
      zoomType: 'x',
      reflow: true,
    },
    title: {
      text: 'Monthly mined'
    },
    xAxis: {
      categories: [],
    },
  };

  constructor(private dataService: DataService) {}

  async ngOnInit() {
    this.chart = new Chart(this.chartOptions);

    this.dataService.plantData$.subscribe((data) => {
      const realData = data.map(item => item.mined);
      const expectedData = data.map(item => item.expect);
      //@ts-ignore
      this.chartOptions.xAxis.categories = data.map(item => `${item.year}/${item.month}`);
      this.chartOptions.series = [
        { name: 'expected', data: expectedData, type: 'column' },
        { name: 'real', data: realData, type: 'column' },
      ];

      this.chart = new Chart(this.chartOptions);
    });
  }
}
