import { powerStation, dataItem } from './data-service';
import * as moment from 'moment'

let powerStations: powerStation[] = [
  { id: 1, name: 'Station 1', budget: 450000 },
  { id: 2, name: 'Station 2', budget: 600000 },
]

enum Months {
  "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
}

interface generateFakeDataArgs {
  plantId: number,
  totalBudget: number,
  today: string
}


function generateFakeData({ plantId, totalBudget, today }: generateFakeDataArgs): dataItem[] {
    const months: number = 12;
    const percentDiff: number = 20;
    let perMonth: number = totalBudget / months;
    let currentDate = moment(today).subtract(months + 1, 'months');
    let results: dataItem[] = [];

    for (let index = 0; index < 12; index++) {
      const shift = Math.random() < 0.5 ? -1 : 1;
      let data: dataItem = {
        plantId,
        month: currentDate.get('month'),
        year: currentDate.get('year'),
        mined: Math.fround(perMonth + ((perMonth / percentDiff) * shift)),
        expect: perMonth,
      };

      results.push(data);

      currentDate = currentDate.add('1', 'month');
    }

    return results;
};



export {
  powerStations,
  generateFakeData,
};
