export interface dataItem {
  plantId: number,
  year: number,
  month: number,
  mined: number,
  expect: number,
}

export interface powerStation {
  id: number,
  name: string,
  budget: number,
}
