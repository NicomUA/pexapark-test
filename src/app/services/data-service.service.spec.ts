import { TestBed } from '@angular/core/testing';

import { DataService } from './data-service.service';
import { powerStation, dataItem } from './data-service';

describe('DataServiceService', () => {
  let service: DataService;
  let powerStations: powerStation[];

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return powerStations', async () => {
     powerStations =  await service.getPowerStations().toPromise();
     expect(powerStations.length).toBeGreaterThan(0);
     powerStations.forEach((plant) => {
       expect(plant.id).toBeTruthy();
       expect(plant.name).toBeTruthy();
       expect(plant.budget).toBeTruthy();
     });
  });

  it('should return one powerStations', async () => {
    powerStations = await service.getPowerStations().toPromise();
    const onePowerStation = service.getOnePowerStation(powerStations[0].id);
    expect(onePowerStation).toBeTruthy();
  });

  it('should return generate fake data  for powerStation', async () => {
    powerStations = await service.getPowerStations().toPromise();
    const plantId = powerStations[0].id;
    let data: dataItem[] = await service.getData(plantId).toPromise();

    expect(data.length).toBeGreaterThanOrEqual(1);
    data.forEach( item => {
      expect(item.plantId).toEqual(plantId);
      expect(item.mined).toBeTruthy();
      expect(item.expect).toBeTruthy();
      expect(item.year).toBeTruthy();
    })
  });
});
