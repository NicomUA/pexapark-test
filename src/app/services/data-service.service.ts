import { Injectable } from '@angular/core';
import { generateFakeData, powerStations  } from './data-source';
import { Observable, BehaviorSubject } from 'rxjs';
import { delay } from 'rxjs/operators'
import { of } from "rxjs";
import { powerStation, dataItem } from './data-service';


function makeDelay(data, toDelay = 2000): Observable<any> {
  return of(data).pipe(delay(toDelay));
}

@Injectable({ providedIn: 'root' })
export class DataService {
  today: string = 'Mon, 03 Sep 2018 00:00:00 GMT';
  private plantDataSource = new BehaviorSubject<dataItem[]>([]);
  public plantData$ = this.plantDataSource.asObservable();

  constructor() { }

  getData(plantId: number): Observable<dataItem[]> {
    const plant = this.getOnePowerStation(plantId);
    if(!plant) throw Error('Wrong plant id');

    let data = generateFakeData({
      plantId: plant.id,
      totalBudget: plant.budget,
      today: this.today
    });

    return makeDelay(data);
  }

  getPowerStations(): Observable<powerStation[]> {
    return makeDelay(powerStations, 2000);
  }

  getOnePowerStation(plantId: number): powerStation {
    return powerStations.find(plant => plant.id == plantId);
  }

  setCurrentStation(plantId): void {
    const plant = this.getOnePowerStation(plantId);
    this.getData(plant.id).subscribe((data) => {
      this.plantDataSource.next(data);
    });

  }
}
